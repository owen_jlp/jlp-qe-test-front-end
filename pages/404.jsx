import Link from "next/link";
import { useEffect } from "react";
import { useRouter } from "next/router";

const NotFound = () => (
  <div className="not-found" style={{margin: '0 auto', width: '90%'}}>
    <h1>Ooops...</h1>
    <h2>That page cannot be found</h2>
    <p>
      Go back to the{" "}
      <Link href="/">
        <a>Homepage</a>
      </Link>
    </p>
  </div>
);

export default NotFound;
